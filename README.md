# Keycloak Guide

## [Part 1. Server Installation and Configuration Guide](https://www.keycloak.org/docs/6.0/server_installation/)

## 1. Overview
Keycloak is a single sign on solution for web apps and RESTful web services. The goal of Keycloak is to make security simple so that it is easy for application developers to secure the apps and services they have deployed in their organization. 
### 1.1. Install Keycloak : 
```
 cd Downloads/
 wget https://downloads.jboss.org/keycloak/6.0.1/keycloak-6.0.1.zip
 unzip keycloak-6.0.1.zip
```
### 1.2. Start Keycloak server : 
```
cd Downloads/
./keycloak-6.0.1/bin/standalone.sh -Djboss.socket.binding.port-offset=100   //通过启动脚本运行单节点模式的 Keycloak Server
```

## 2. Network Setup
### 2.1. Bind Addresses
```
cd Downloads/
./keycloak-6.0.1/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Djboss.bind.address=192.168.1.136
```
**NOTE** : `-Djboss.bind.address=192.168.1.136` = `-b 192.168.1.136`

### 2.2. Setting up HTTPS/SSL
   1. Obtaining or generating a keystore that contains the private key and certificate for SSL/HTTP traffic
   2. Configuring the Keycloak server to use this keypair and certificate.


## [Part 2. Server Administration Guide](https://www.keycloak.org/docs/6.0/server_admin/)
## 1. Server Initialisation
* create an initial admin account using script : 
```
cd Downloads/
./keycloak-6.0.1/bin/add-user-keycloak.sh -r master -u <username> -p <password>
```

## 2. Admin Console
### 2.1. [Email settings](https://codehumsafar.wordpress.com/2018/09/23/keycloak-configure-and-test-email-settings-for-realm/)
To enable Keycloak to send emails you need to provide Keycloak with your SMTP server settings. This is configured per realm. 

We will use Gmail account to configure and test the Email settings for the selected Realm of your KeyCloak Server.

Make sure [the admin has email configured.](https://codehumsafar.wordpress.com/2018/09/23/keycloak-how-to-assign-email-address-to-admin-account/)

Note that if you are using [Gmail account](https://www.jotform.com/help/392-How-to-Use-Your-Gmail-Account-as-Your-Email-Sender-via-SMTP), make sure you have enable **[Allow less secure apps for your Gmail account](https://codehumsafar.wordpress.com/2018/09/23/how-to-enable-less-secure-apps-in-gmail-account/)**.

![](./figs/secure.png)

Note that if two-step verification is enabled for your Gsuite or Gmail account, Less Secure App is unavailable for use.  You will have to get an **App Specific Password** for Jotform for the SMTP to work : 

**`avcezchpfzemjacm`**

Login to KeyCloak Server Admin Console with your admin credentials.

Select Realm. For example, here “test-kube” realm is selected.

Click the Realm Settings in the left pane.

Click the Email tab. Enter the information : 

![](./figs/email.png)

The test mail that you will receive will look like below:

![](./figs/mailTest.png)

### 2.2 Internationalization
Every UI screen is internationalized in Keycloak. The default language is English, but if you turn on the `Internationalization` switch on the `Theme` tab you can choose which locales you want to support and what the default locale will be. 
![](./figs/internation.png)

## 3. User management
### 3.1. reCAPTCHA Support
To safeguard registration against bots, Keycloak has integration with Google reCAPTCHA. 

To enable this you need to first go to [Google Recaptcha Website](https://developers.google.com/recaptcha/intro) and create an API key so that you can get your reCAPTCHA site key and secret.

![](./figs/recaptcha.png)

```
6LcIA60UAAAAAKe7WIs5ww2bUoFuPApNdxp8hy4s    // 在您的网站提供给用户的 HTML 代码中使用此网站密钥
6LcIA60UAAAAAJVcVIgETSEZfI8dgax6XTtk3mVR     // 此密钥用于您的网站和 reCAPTCHA 之间的通信
```

Next, Set the 'reCAPTCHA' requirement to Required by clicking the appropriate radio button.
![](./figs/require.png)

Next, you have to enter in the reCAPTCHA site key and secret that you generated at the Google reCAPTCHA Website. Click on the 'Actions' button that is to the right of the reCAPTCHA flow entry, then "Config" link, and enter in the reCAPTCHA site key and secret on this config page.

![](./figs/config.png)

The final step to do is to change some default HTTP response headers that Keycloak sets. Keycloak will prevent a website from including any login page within an iframe. This is to prevent clickjacking attacks. You need to authorize Google to use the registration page within an iframe. Go to the Realm Settings left menu item and then go to the Security Defenses tab. You will need to add https://www.google.com to the values of both the X-Frame-Options and Content-Security-Policy headers.

![](./figs/securityDefense.png)

## 4. Authentication
### 4.1. Password policy
Click on the `Authentication` left menu item and go to the `Password Policy` tab. Choose the policy you want to add in the right side drop down list box.

## Deploy keycloak in kubernetes 
```
docker pull jboss/keycloak
```